from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]


# we already have the model, just create form here, usually add the word Form behind it.
class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ["name"]


# create form, then create view but make sure you import the new form
class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
