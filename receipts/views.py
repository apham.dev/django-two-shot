from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.
@login_required
def receipt_list(request):
    all_the_lists = Receipt.objects.filter(purchaser=request.user)
    context = {"all_the_lists": all_the_lists}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
        return redirect("home")
    else:
        form = ReceiptForm()
        ## filtering only where we can see this account
        form.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=request.user
        )
        form.fields["account"].queryset = ExpenseCategory.objects.filter(
            owner=request.user
        )

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    user_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {"user_list": user_list}
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {"account_list": account_list}
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create_categories.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
